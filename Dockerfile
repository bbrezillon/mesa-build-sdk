FROM debian:bullseye
ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
ENV NSS_UNKNOWN_HOME=/home/user
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y \
      build-essential \
      libnss-unknown \
      libncurses5-dev \
      rsync \
      cpio \
      python \
      unzip \
      bc \
      wget \
      git \
      vim-common

RUN apt-get update && \
    apt-get install -y \
      binutils-aarch64-linux-gnu \
      bison \
      cmake \
      flex \
      g++-aarch64-linux-gnu \
      gcc-aarch64-linux-gnu \
      libglib2.0-dev-bin \
      meson \
      pkg-config \
      python3-setuptools \
      python3-mako \
      libwayland-dev \
      wayland-protocols \
      libwayland-egl-backend-dev \
      libdrm-dev

CMD ["/bin/bash"]
