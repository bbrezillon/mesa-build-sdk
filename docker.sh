#!/bin/bash

IMAGE_TAG=registry.gitlab.collabora.com/collabora/gst-build-sdk/gst-build-sdk:main
MUID=`id -u`
MGID=`id -g`
MUID="0"
MGID="0"

case "$1" in
  run)
    podman run -it \
    --net=host \
    -v $SYSROOT:$SYSROOT \
    -w $SYSROOT \
    -u $MUID:$MGID \
    --mount type=tmpfs,destination=/home/user \
     --security-opt label=disable \
    $IMAGE_TAG \
    /bin/bash
    ;;
  build)
    podman build -t $IMAGE_TAG .
    ;;
  *)
    echo "Usage: $0 {run|build}"
    exit 1
esac

